#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

import paho.mqtt.client as mqtt

import db
import util
import json
import os

config = util.load_config()
broker_host = config["mqtt"]["host"]
broker_port = config["mqtt"]["port"]
mqtt_pass = os.environ['mqtt_pass']


class Watcher:
    def on_message(self, client, userdata, message):
        json_payload = message.payload.decode()
        print("Recibido: {}".format(json_payload))
        dict_payload = json.loads(json_payload)

        db.MedicionSensores.insert(
            dict_payload["oid"],
            dict_payload["temp"],
            dict_payload["hum"],
            datetime.datetime.now(),
        )

    def start(self):
        client = mqtt.Client()
        client.username_pw_set(username="esp8266",password=mqtt_pass)
        client.connect(broker_host, broker_port)
        
        print("starting watcher at {}:{}".format(broker_host, broker_port))

        topic = config["mqtt"]["sensor_topic"]
        client.subscribe(topic)
        client.message_callback_add(topic, self.on_message)

        client.loop_forever()


if __name__ == "__main__":
    w = Watcher()
    w.start()
