#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
import json

from flask import Flask, render_template, request, redirect, abort, session

import db
import util
import watcher


app = Flask(__name__)

config = config = util.load_config()
app.secret_key = "Auzfj0JRnF/iHQhn6qgMlaCNQQauVndSFGB3+qs6AyM"


@app.route("/")
def v_index():
    return render_template("index.html")


@app.route("/login", methods=["GET", "POST"])
def v_login():
    if request.method == "GET":
        return render_template("login.html")

    if "password" not in request.form or "username" not in request.form:
        abort(400)

    if (
        request.form["password"] == config['login']['pass']
        and request.form["username"] == config['login']['user']
    ):
        session["logged"] = "logged"
        return redirect("dashboard")
    else:
        abort(401)


@app.route("/dashboard")
def v_dash():
    if not session.get("logged"):
        abort(401)

    global config

    muros = db.Muro.get_all()
    map(lambda x: x["fecha"].replace(microsecond=0), muros)

    return render_template(
        "dash.html", muros=muros, defaults=config["defaults"]
    )


@app.route("/logoff")
def v_logoff():
    session["logged"] = False
    return redirect("/")


@app.route("/muro/new", methods=["POST"])
def a_muro_new():
    if not session.get("logged"):
        abort(401)

    if "ubicacion" not in request.form:
        abort(400)

    db.Muro.insert(
        request.form["ubicacion"],
        request.form.get("frecuencia_regado"),
        request.form.get("duracion_regado"),
    )

    return redirect("/dashboard")


@app.route("/muro/delete", methods=["POST"])
def a_muro_delete():
    if not session.get("logged"):
        abort(401)

    if "oid" not in request.form:
        abort(400)

    db.Muro.delete(request.form["oid"])

    return redirect("/dashboard")

@app.route("/mediciones/<oid>")
def a_mediciones(oid):
    if not session.get("logged"):
        abort(401)

    med = db.MedicionSensores.get_all_oid(oid)

    return render_template('mediciones.html', oid=oid, mediciones=med)


if __name__ == "__main__":
    app.run(debug=True)