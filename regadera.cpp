#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <dht11.h>
dht11 DHT11;
#define DHT11PIN 4

// Update the serial display every 60 seconds.
const float supdate = 10000;
 
const char* ssid = "moto X4 2200";
const char* password =  "camilo123";
const char* mqttServer = "ctrd.me";
const int mqttPort = 1883;
const char* mqttUser = "esp8266";
const char* mqttPassword = "plantas";
const char* oid = "5ce6dbe4b42eebf5cbd0a277";
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
 
  Serial.begin(115200);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }

}
void loop(){
  Serial.println("");
  int chk = DHT11.read(DHT11PIN);


  Serial.print("Humidity: ");
  Serial.print("\t");
  Serial.print(DHT11.humidity);
  Serial.println(" %");

  Serial.print("Temperature: ");
  Serial.print("\t");
  Serial.print(DHT11.temperature);
  Serial.println(" C");

  char* payload = ("{'oid': '"+ oid +"'}")

  Serial.println(payload);

  client.publish("sensor_update", &payload);

  delay(supdate);
}
