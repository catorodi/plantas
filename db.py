#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pymongo
import datetime

from bson.objectid import ObjectId


def get_db():
    client = pymongo.MongoClient("localhost", 27017)
    return client.plantas


class Muro:

    """
    #ID numérica
    id int

    #ubicacion
    ubicacion string

    #frecuencia de regado, cada cuantos horas se regara
    freq_regado int

    #duracion de cada evento de regado, en segundos
    duracion_regado int
    
    #fecha de creacion
    fecha date
    """

    @staticmethod
    def get_all():
        return list(get_db().muro.find())

    @staticmethod
    def count():
        return get_db().muro.count_documents({})

    @staticmethod
    def get_one(oid):
        return get_db().muro.find_one({"_id": ObjectId(oid)})

    @staticmethod
    def insert(
        ubicacion, frecuencia_regado, duracion_regado, fecha=datetime.datetime.utcnow()
    ):

        get_db().muro.insert(
            {
                "ubicacion": ubicacion,
                "frecuencia_regado": frecuencia_regado,
                "duracion_regado": duracion_regado,
                "fecha": fecha,
            }
        )

    @staticmethod
    def delete(oid):
        get_db().posts.delete_one({"_id": ObjectId(oid)})


class MedicionSensores:
    """
        Clase que describe una toma de medidas de los sensores

        oid_muro oid
        id de muro

        temp int
        temperatura ambiental, en c

        humedad
        humedad ambiental, en porcentaje de humedad relativa

        fecha
        fecha de la medicion
    """

    @staticmethod
    def get_all():
        return list(get_db().mediciones.find())

    @staticmethod
    def get_all_oid(oid):
        return list(get_db().mediciones.find({"oid_muro": oid}))

    @staticmethod
    def count():
        return get_db().mediciones.count_documents({})

    @staticmethod
    def count_by_oid():
        return get_db().mediciones.count_documents({"_id": ObjectId(oid)})

    @staticmethod
    def get_one(oid):
        return get_db().mediciones.find_one({"_id": ObjectId(oid)})

    @staticmethod
    def insert(oid, temp, humedad, fecha):

        get_db().mediciones.insert(
            {"oid_muro": oid, "temp": temp, "humedad": humedad, "fecha": fecha}
        )

    @staticmethod
    def get_last(oid):
        return get_db().mediciones.find_one(
            {"_id": ObjectId(oid)}, sort=[("fecha", pymongo.ASCENDING)]
        )

    @staticmethod
    def delete(oid):
        get_db().mediciones.delete_one({"_id": ObjectId(oid)})
