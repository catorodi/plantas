function delete_muro(oid) {
    form_data = new FormData();
    form_data.append('oid', oid);

    var xhr = new XMLHttpRequest();
    xhr.onload = function(){ console.log(xhr.responseText); } // success case
    xhr.onerror = function(){ console.log(xhr.responseText); } // failure case
    xhr.open('POST', 'muro/delete/', true);
    xhr.send(form_data);

    return false;
}
