#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import random
import json
import sys

import paho.mqtt.client as mqtt

oid =  sys.argv[1]
broker = {"host": "ctrd.me", "port" : 1883}


def get_temp():
    return round(random.uniform(10,35)) 


def get_hum():
    return round(random.uniform(50,90)) 


if __name__ == "__main__":
    client = mqtt.Client(oid)
    client.username_pw_set(username="esp8266",password="plantas")
    client.connect(broker['host'], port=broker['port'])

    while True:
        payload = json.dumps({
            'oid' : oid,
            'temp' : get_temp(),
            'hum' : get_hum()
            })
        
        print("pushing {}".format(payload))
        client.publish("sensor_update", payload)

        time.sleep(10)



